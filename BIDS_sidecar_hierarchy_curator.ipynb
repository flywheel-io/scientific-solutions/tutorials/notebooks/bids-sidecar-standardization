{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b48d6828",
   "metadata": {},
   "source": [
    "# BIDS sidecar standardization notebook\n",
    "\n",
    "With Flywheel v18+, the JSON sidecar data is now stored in the JSON sidecar files and not added as metadata to the `file.info` object <br> \n",
    "of the associated NIfTI file.  The decision to change how the JSON sidecar data are stored was done to avoid confusion and adopt a more <br> BIDS-like way of handling the NIfTI/JSON file pairs.  This update, however, can cause issues with running more recent versions of <br>\n",
    "`dcm2niix` on older projects.  The code in this notebook can be used to take projects curated with the JSON sidecar data stored as <br>\n",
    "metadata and convert them to storing the JSON sidecar data in JSON sidecar files.\n",
    "\n",
    "This notebook contains code and examples for:\n",
    "- changing the BIDS curation method of an existing Flywheel project\n",
    "- determining the current BIDS curation method of a Flywheel project\n",
    "- setting the BIDS curation method for a new Flywheel project\n",
    "\n",
    "There can also be issues with using the classic Flywheel CLI v18+ to ingest existing BIDS datasets into newly created projects.  Instead <br>\n",
    "of reverting to an older version of the classic Flywheel CLI, this notebook contains a short workflow to automatically set the new project <br>\n",
    "to the correct curation method so that `fw ingest bids` runs without error.\n",
    "\n",
    "All of the code in this notebook can be run directly from the notebook.  For changing the BIDS curation method of an existing Flywheel <br>\n",
    "project, it is also possible to upload the first python code block as a project attachment and run it using the `hierarchy-curator` gear.\n",
    "\n",
    "### Getting started with the notebook:\n",
    "**Note**: The notebook requires both the Flywheel SDK and Flywheel Gear Toolkit packages installed via `pip`.\n",
    "1. Run the first two code blocks (The curator script and helper function) to import the necessary packages and define all of the functions <br>\n",
    "used in the specific workflows.\n",
    "2. Run Step 1 to instantiate a Flywheel Client instance.  (If you are not otherwise logged in via the Flywheel CLI, you may need to <br>\n",
    "provide your `api-key`.)\n",
    "3. Run Step 2 to create the Hierarchy Curator object.\n",
    "4. From here, navigate to the specific workflow you are interested in.\n",
    "\n",
    "#### Workflow 1: Curate an existing project\n",
    "Workflow 1 covers changing the curation of an existing project.  The two examples cover changing the curation of a project to the \"old\" <br>\n",
    "way, where JSON sidecar data is stored as file metadata and changing the curation of a project to the \"new\" way, where JSON sidecar <br>\n",
    "data is stored in the JSON sidecar files.\n",
    "\n",
    "#### Workflow 2: Checking the curation status of an existing project\n",
    "Workflow 2 includes examples of how to determine the curation status of both all of the projects on an instance or of a specific <br>\n",
    "project on an instance.  **Note**: When running Step 2a, the projects found and listed will be determined on your user permissions.\n",
    "\n",
    "#### Workflow 3: Set up a new project to be curated\n",
    "Workflow 3 covers three examples of how to curate a newly created project.  As noted above, if you are planning on running <br>\n",
    "`fw ingest bids` to import an existing BIDS dataset, it is important to run Step 3a on the new project before running <br>\n",
    "`fw ingest bids`.  This workflow also contains an example of how to clear the BIDS curation from a project.\n",
    "\n",
    "### Getting started with the `hierarchy-curator` gear\n",
    "1. Copy the python code in The curator script code block into a new file and upload it as an attachment to a project.\n",
    "2. Edit the first block of variables (`UPDATE_METADATA`, `MAKE_SIDECAR`, `REPORT_ONLY`, `WAY`) to the desired values.\n",
    "3. Launch the `hierarchy-curator` gear (analysis) from the desired container (`project`, `subject`, `session`, or `acquisition`) and <br>\n",
    "supply the python script from the project attachments as the input `curator`.  \n",
    "4. Run the gear and check the results.\n",
    "\n",
    "### FAQs\n",
    "1. I changed the curation of my project from the \"old\" way to the \"new\" way, but I'm still seeing the metadata in the `file.info` object of the <br> NIfTI images?  \n",
    "    **Answer**: This is correct.  The curation script will create the JSON files (if missing) with the information from the metadata, <br>\n",
    "    and any BIDS export or BIDS app gears will ignore the metdata in favor of the JSON sidecar files.  The curation script will not <br>\n",
    "    delete the existing metadata, as we do not know if your project has custom `file.info` metadata fields you would not want deleted. <br>\n",
    "2. When using `fw ingest bids` to import a BIDS dataset into a new project I am getting the following error:\n",
    "    ```\n",
    "    Error: The argument save_sidecar_as_metadata has been passed in as True but there is no dataset_description data in  \n",
    "    project.info.BIDS.  Either save_sidecar_as_metadata should be False (the default), or the project should be curated for  \n",
    "    BIDS with save_sidecar_as_metadata set to True.\n",
    "    exit status 1\n",
    "    ``` \n",
    "    **Answer**: See the text above about issues with using recent versions of the classic CLI to ingest BIDS data.  Either run Workflow <br>\n",
    "    3 from this notebook to set the project curation to the \"old\" way, or use the `hierarchy-curator` gear to run the curator script at <br>\n",
    "    the project level, setting the project to be curated the \"old\" way."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16335167-17d5-474b-93cd-87cd7daa2e94",
   "metadata": {},
   "source": [
    "## The curator script\n",
    "\n",
    "The following code can be run in this jupyter notebook or placed on a project and run with the Hierarchy Curator gear.\n",
    "\n",
    "The overall flow of the curator is to find the submitted container's level and check all the acquisitions belonging to the container to ensure that all the sidecar information is either in metadata (the \"old way\" that Flywheel handled sidecar information) or in a proper sidecar, as would be created by dcm2niix (the \"new way\" that Flywheel stores sidecar data).\n",
    "\n",
    "One can give the curator any level container (i.e., project, subject, session, or acquisition) and the script will check all associated acquisitions.\n",
    "\n",
    "As you begin familiarizing yourself with the script, you may find it helpful to select a specific acquisition and run the code in its own cell. See the final cells in this notebook for examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "5558529f",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"An example curation script to check BIDS metadata or sidecars at various levels of the hierarchy.\n",
    "\n",
    "This script can be named sidecar_curator.py (or similar) and placed on a project to be used with the Hierarchy Curator gear.\n",
    "\"\"\"\n",
    "import flywheel\n",
    "import io\n",
    "import json\n",
    "import logging\n",
    "import os\n",
    "import re\n",
    "import tempfile\n",
    "\n",
    "from collections import defaultdict\n",
    "from pathlib import Path\n",
    "\n",
    "from flywheel_gear_toolkit.utils.curator import HierarchyCurator\n",
    "\n",
    "# IF USING AS THE HC GEAR SCRIPT, CHANGE THE DEFAULT VALUES TO MATCH THE DESIRED CURATION STYLE.\n",
    "    # OLD WAY: update_metadata=True, make_sidecar=False, way = \"old\"\n",
    "    # NEW WAY: update_metadata=False, make_sidecar=True, way = \"new\"\n",
    "    # RESET: update_metadata=False, make_sidecar=False, way = \"none\"\n",
    "UPDATE_METADATA = True\n",
    "MAKE_SIDECAR = False\n",
    "REPORT_ONLY = False     # If True, the script will only log what it would do, not actually do it\n",
    "WAY = \"old\"             # \"old\", \"new\", or \"none\"\n",
    "\n",
    "# Set the default values for the BIDS metadata\n",
    "DATASET_DESCRIPTION = {'template': 'project',\n",
    " 'rule_id': 'bids_project',\n",
    " 'Name': 'BIDS_multi_session',\n",
    " 'BIDSVersion': '1.0.2',\n",
    " 'License': '',\n",
    " 'Authors': [],\n",
    " 'Acknowledgements': '',\n",
    " 'HowToAcknowledge': '',\n",
    " 'Funding': [],\n",
    " 'ReferencesAndLinks': [],\n",
    " 'DatasetDOI': ''}\n",
    "\n",
    "NEW_WAY_PROJECT_INFO = {'Sidecar': 'data is in json sidecar, not file.info',\n",
    "  'rule_id': 'bids_project',\n",
    "  'template': 'project'}\n",
    "\n",
    "# Configure the logger; CHANGE level to logging.DEBUG for more messages\n",
    "logging.basicConfig(level=logging.DEBUG, format='%(levelname)s: %(message)s')\n",
    "log = logging.getLogger()\n",
    "\n",
    "\n",
    "def split_extension(filename: str):\n",
    "    \"\"\"Remove flexible number of extensions.\n",
    "\n",
    "    Imaging files and archives tend to have flexible numbers of extensions.\n",
    "    This simple method deals with all extensions to return the most basic\n",
    "    basename. e.g., /path/to/my/output/archive.tar.gz returns 'archive'\n",
    "\n",
    "    Note: don't extend this method to all filenames, since 1.2.34.567.nii.gz\n",
    "    or another UID-esque name would return problematically.\n",
    "    :param filename (str): any filename with or without multiple extensions\n",
    "    :return filename (str) Non-UID basename without any extensions\n",
    "    :return ext (str) The combined suffixes from the original filename\n",
    "    \"\"\"\n",
    "    ext = \".\" + \".\".join(Path(filename).name.split(\".\")[1:])\n",
    "    filename = str(Path(filename).stem.split(\".\")[0])\n",
    "    if len(filename) < 4:\n",
    "        log.warning(f\"Is {filename} the correct filename without an extension?\")\n",
    "    return filename\n",
    "\n",
    "\n",
    "def populate_metadata(container, sidecar_contents, report_only=False):\n",
    "    \"\"\"Use sidecar info to populate/update the file.info field\"\n",
    "    \n",
    "    container: container object of any of the Flywheel container types\n",
    "    report_only: log how the container is curated\n",
    "    \"\"\"\n",
    "    data = container.info\n",
    "    orig_data = data.copy()\n",
    "    data.update(sidecar_contents)\n",
    "    \n",
    "    if orig_data != data:\n",
    "        if report_only:\n",
    "            try:\n",
    "                name = container.label\n",
    "            except:\n",
    "                name = container.name\n",
    "            log.info(f'Need to update metadata: {name}')\n",
    "        else:\n",
    "            #log.debug(f'Adding {sidecar_contents}')\n",
    "            container.update_info(data)\n",
    "    else:\n",
    "        log.info('Metadata already exists')\n",
    "\n",
    "\n",
    "def create_sidecar(client, acquisition, file, report_only=False):\n",
    "    file_ = file.reload()\n",
    "    metadata = file_.info\n",
    "    log.debug(f\"Cleaning {metadata}\")\n",
    "    sidecar_md = clean_metadata(metadata,['header','BIDS', 'qc'])\n",
    "    if len(sidecar_md) > 0:\n",
    "        sidecar_name = split_extension(file_.name) + '.json'\n",
    "        json_string = json.dumps(sidecar_md, sort_keys=True, indent=4)\n",
    "        if report_only:\n",
    "            try:\n",
    "                name = acquisition.label\n",
    "            except:\n",
    "                name = acquisition.name\n",
    "            log.info(f\"Need to create sidecar json: {name}\")\n",
    "        else:\n",
    "            log.info(f'Pushing {sidecar_name} to platform')\n",
    "            with io.StringIO(json_string) as fd:\n",
    "                fsize = len(json_string)\n",
    "                client.upload_file_to_acquisition(acquisition.id, flywheel.FileSpec(sidecar_name, fd, size=fsize))\n",
    "\n",
    "            log.info(f\"Removing sidecar keys from file.info\")\n",
    "            new_md = clean_metadata(file_.info,sidecar_md.keys())\n",
    "            log.debug(new_md)\n",
    "            file_.replace_info(new_md)\n",
    "    else:\n",
    "        log.info('No applicable metadata to create a sidecar')\n",
    "\n",
    "\n",
    "def clean_metadata(metadata, keys_to_delete):\n",
    "    \"\"\"\n",
    "    Recursively delete a extraneous keys from the info dictionary.\n",
    "\n",
    "    In this case, clean the dictionary so that only sidecar data remains.\n",
    "\n",
    "    Parameters:\n",
    "    metadata (dict): The dictionary from which to delete the key.\n",
    "    key_to_delete (str): The key to delete.\n",
    "    \"\"\"\n",
    "    if not isinstance(metadata, dict):\n",
    "        return metadata\n",
    "    \n",
    "    new_dict = {k: clean_metadata(v, keys_to_delete) for k, v in metadata.items() if k not in keys_to_delete}\n",
    "    \n",
    "    return new_dict\n",
    "\n",
    "def set_project_old_new_none(client, project_id, way, report_only):\n",
    "    if report_only:\n",
    "        log.info(f\"Would set project info for {project_id} to {way}\")\n",
    "        return\n",
    "    else:\n",
    "        log.info(f\"Setting project info for {project_id} to {way}\")\n",
    "        if way == \"old\":\n",
    "            result = client.set_project_info(project_id, {\"BIDS\": DATASET_DESCRIPTION})\n",
    "        elif way == \"new\":\n",
    "            result = client.set_project_info(project_id, {\"BIDS\": NEW_WAY_PROJECT_INFO})\n",
    "        elif way == \"none\":\n",
    "            result = client.delete_project_info_fields(project_id, [\"BIDS\"])\n",
    "        else:\n",
    "            log.info(f\"set_project_old_new_none(): way must be 'old', 'new', or 'none', not {way}.\")\n",
    "    return result\n",
    "\n",
    "\n",
    "class Curator(HierarchyCurator):\n",
    "    \"\"\"A curation script to clear metadata on based on a given container type.\n",
    "    \n",
    "    For each container type that has a child container, the curator will descend\n",
    "    into the child container(s) and perform the metadata clearing. The container,\n",
    "    itself, will have its metadata cleared after the child containers are cleared.\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, **kwargs):\n",
    "        super().__init__(**kwargs)\n",
    "        try:\n",
    "            # Use platform/gear run settings\n",
    "            api_key = self.context.get_input(\"api-key\").get(\"key\")\n",
    "            self.client = flywheel.Client(api_key, root=True)\n",
    "        except AttributeError as e:\n",
    "            # Use local settings\n",
    "            self.client = fw\n",
    "              \n",
    "    def curate_acquisition(self, acquisition: flywheel.Acquisition, \n",
    "                           update_metadata=UPDATE_METADATA, \n",
    "                           make_sidecar=MAKE_SIDECAR, \n",
    "                           report_only=REPORT_ONLY):\n",
    "        \"\"\"Curate an acquisition.\n",
    "        \n",
    "        This method is the essential part of correcting/curating the selected containers.\n",
    "        \"\"\"\n",
    "        log.info(f\"Curating acquisition {acquisition.label}\")\n",
    "        # Locate the applicable files\n",
    "        nifti_files = [f for f in acquisition.files if 'nii' in f.name]\n",
    "        \n",
    "        try:\n",
    "            # Find the shortest name to call the basename and avoid prefixed, possibly processed files\n",
    "            basename = split_extension(min([f.name for f in nifti_files], key=len))\n",
    "            log.debug(f\"Processing matches for {basename}\")\n",
    "            # Find matching sidecar\n",
    "            sidecar = [f for f in acquisition.files if f.name.endswith('json') and f.name.startswith(basename)]\n",
    "        except ValueError:\n",
    "            log.debug(f\"Did not find a nifti amongst: {[f.name for f in acquisition.files]}\")\n",
    "\n",
    "        for file_ in nifti_files:\n",
    "            log.info(f\"Curating file: {file_.name}\")\n",
    "            # Match \"old way\", where metadata is on file.info, not in a sidecar.\n",
    "            if update_metadata and sidecar:\n",
    "                log.debug(f'Sidecar: {sidecar[0].name}')\n",
    "                sidecar_contents = json.loads(acquisition.read_file(sidecar[0].name).decode('utf-8'))\n",
    "                populate_metadata(file_, sidecar_contents, report_only)\n",
    "            elif update_metadata:\n",
    "                log.error(f'Cannot upload metadata, because there was no matching sidecar.')\n",
    "            \n",
    "            # Match \"new way\", where the sidecar stores the expected data\n",
    "            if make_sidecar:\n",
    "                if sidecar:\n",
    "                    # Don't overwrite an existing sidecar. Assume existing sidecar was generated with dcm2niix\n",
    "                    log.info(f'Already had {sidecar[0].name}.')\n",
    "                else:\n",
    "                    create_sidecar(self.client, acquisition, file_, report_only)\n",
    "                # it would be nice here to delete the sidecar data from the NIfTI file metadata, but\n",
    "                # how can we know what the keys are?  We don't just want to delete everything that is there.\n",
    "            \n",
    "            \n",
    "    def curate_session(self, sess: flywheel.Session, update_metadata=UPDATE_METADATA, \n",
    "                           make_sidecar=MAKE_SIDECAR,report_only=REPORT_ONLY):\n",
    "        \"\"\"Curate a session.\"\"\"\n",
    "        log.info(f\"Curating session {sess.label}\")\n",
    "        for acq in sess.acquisitions():\n",
    "            self.curate_acquisition(acq, update_metadata, make_sidecar,  report_only)\n",
    "\n",
    "            \n",
    "    def curate_subject(self, subj: flywheel.Subject, update_metadata=UPDATE_METADATA, \n",
    "                           make_sidecar=MAKE_SIDECAR,report_only=REPORT_ONLY):\n",
    "        \"\"\"Curate a subject.\"\"\"\n",
    "        log.info(f\"Curating subject {subj.label}\")\n",
    "        for sess in subj.sessions():\n",
    "            self.curate_session(sess, update_metadata, make_sidecar,  report_only)\n",
    "                \n",
    "    def curate_project(self, proj: flywheel.Project, update_metadata=UPDATE_METADATA, \n",
    "                           make_sidecar=MAKE_SIDECAR, way=WAY, report_only=REPORT_ONLY):\n",
    "        \"\"\"Curate a project.\"\"\"\n",
    "        log.info(f\"Curating project {proj.label}\")\n",
    "        for subj in proj.subjects():\n",
    "            self.curate_subject(subj, update_metadata, make_sidecar, report_only)\n",
    "        # Set the project metadata to the desired state\n",
    "        set_project_old_new_none(self.client, proj.id, way, report_only)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1497caf3-1309-40f7-bc14-94cd51b7bdba",
   "metadata": {},
   "source": [
    "## Helper function to find out how a project is currently curated\n",
    "\n",
    "The following function will let you know if a project is curated for BIDS using the \"old way\" (sidecar data is storedin NIfTI file metadata), the \"new way\" (sidecar data is stored in sidecars), or not at all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "66494125-35f2-4192-8e9f-1067ea9f7989",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_project_bids_status(project, verbose=False):\n",
    "    msg = f\"Project {project.label} does not have 'BIDS' in project.info\"\n",
    "    status = None\n",
    "    if \"BIDS\" in project.info:\n",
    "        if (\"Acknowledgements\" in project.info[\"BIDS\"]):\n",
    "            msg = f\"Project {project.label} is curated the 'old way' (sidecar data is in NIfTI file metadata)\"\n",
    "            status = \"old\"\n",
    "        else:\n",
    "            msg = f\"Project {project.label} is curated the 'new way' (sidecar data is in sidecars)\"\n",
    "            status = \"new\"\n",
    "    if verbose:\n",
    "        print(msg)\n",
    "    return status    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f322aaf3-7cf5-437d-b119-ec68ee4532c3",
   "metadata": {},
   "source": [
    "## 1. Instantiate the Flywheel Client"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "bfe56129",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Logged into https://latest.sse.flywheel.io/api\n"
     ]
    }
   ],
   "source": [
    "# If you are logged in to a Flywheel instance, this will instantiate a Flywheel client using your api key:\n",
    "fw = flywheel.Client()\n",
    "\n",
    "# If you store your key in an environment variable, use the following line to find and load the aliased key.\n",
    "# fw = flywheel.Client(os.environ.get('my_aliased_key'))\n",
    "\n",
    "# Sanity check\n",
    "print(f\"Logged into {fw.get_config()['site']['api_url']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71dfcab2-90a7-4db8-8cfc-21fadefcdfd1",
   "metadata": {},
   "source": [
    "## 2. Instantiate the hierarchy curator object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "a8173071",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiate the Hierarchy Curator\n",
    "hc = Curator()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0591f955-3e12-4291-92d5-22796b4eaa37",
   "metadata": {},
   "source": [
    "## Workflow 1: Curate an existing project to match the intended format (metadata or sidecar)\n",
    "For a project containing curated BIDS data, run the `curate_project` function to change the curation of the project.  All acquisitions in the project will be checked and modified IF needed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e86d5f14",
   "metadata": {},
   "source": [
    "### 1a. Curate project the \"new\" way (metadata stored in JSON sidecar files).\n",
    "Setting `update_medata` to `False` and `make_sidecar` to `True` will create sidecar files where missing using the metadata in \n",
    "the nifti files.  The metadata in the nifti files will not be deleted, however, it will be ignored when running BIDS apps gears and \n",
    "exporting the project as a BIDS dataset.\n",
    "\n",
    "Change the project hash to your desired project hash.  You can find your project hash either using API or SDK calls or grabbing it from \n",
    "the URL in your user instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "9b3f9cf5",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO: Curating project Levitas_Copy\n",
      "INFO: Curating subject 10462@thwjames_OpenScience\n",
      "INFO: Curating session 2020-01-22 14_29_46\n",
      "INFO: Would set project info for 646e508a65932d99265df2f4 to new\n"
     ]
    }
   ],
   "source": [
    "# Command for creating sidecars for ALL files in a project.\n",
    "# Change the hash to your project hash\n",
    "# Use report_only, if you wish to see what would change, but not actually change anything.\n",
    "project = fw.get('646e508a65932d99265df2f4')\n",
    "hc.curate_project(project, update_metadata=False, make_sidecar=True, way='new', report_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e15ea8e3",
   "metadata": {},
   "source": [
    "### 1b. Curate project the \"old\" way (metadata stored in file.info custom metadata object).\n",
    "Setting `update_medata` to `True` and `make_sidecar` to `False` will create custom `file.info` metadata using the information contained \n",
    "in the JSON sidecars.  The existing sidecar files will not be deleted, however, they will be ignored when running BIDS apps gears and \n",
    "exporting the project as a BIDS dataset.\n",
    "\n",
    "Change the project hash to your desired project hash.  You can find your project hash either using API or SDK calls or grabbing it from \n",
    "the URL in your user instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "b79b13a4-ab5a-4495-ba1f-296383b5df24",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO: Curating project Levitas_Copy\n",
      "INFO: Curating subject 10462@thwjames_OpenScience\n",
      "INFO: Curating session 2020-01-22 14_29_46\n",
      "INFO: Would set project info for 646e508a65932d99265df2f4 to old\n"
     ]
    }
   ],
   "source": [
    "# Command for creating metadata from sidecars for ALL files in a project.\n",
    "# Change the hash to your project hash\n",
    "# Use report_only, if you wish to see what would change, but not actually change anything.\n",
    "project = fw.get('646e508a65932d99265df2f4')\n",
    "hc.curate_project(project, update_metadata=True, make_sidecar=False, way='old', report_only=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e2f3ba4",
   "metadata": {},
   "source": [
    "## Workflow 2: Checking the curation of a project on an instance\n",
    "\n",
    "The following code blocks can be used to print out the curation status of all projects on an instance (depending on user permissions) \n",
    "or the curation status of a specific project."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f364ae01",
   "metadata": {},
   "source": [
    "### 2a. Print curation status of all projects on instance\n",
    "The projects found will depend on the permissions of the user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2199b3ea-39ad-4253-b846-f340aadf1e09",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Looking through all projects for 'project.info[\\\"BIDS\\\"]`\")\n",
    "\n",
    "nones = []\n",
    "olds = []\n",
    "news = []\n",
    "for proj in fw.projects.iter_find():\n",
    "    group_project = f\"{proj.parents['group']}/{proj.label}\"\n",
    "    status = get_project_bids_status(proj)\n",
    "    if status == \"old\":\n",
    "        olds.append(group_project)\n",
    "    elif status == \"new\":\n",
    "        news.append(group_project)\n",
    "    else:\n",
    "        nones.append(group_project)\n",
    "\n",
    "print(\"Projects that do not have 'BIDS' in project.info\")\n",
    "for gp in nones:\n",
    "    print(\"   \" + gp)\n",
    "\n",
    "print(\"\\nProjects curated the 'old way' (sidecar data is in NIfTI file metadata)\")\n",
    "for gp in olds:\n",
    "    print(\"   \" + gp)\n",
    "\n",
    "print(\"\\nProjects curated the 'new way' (sidecar data is in sidecars, not metadata)\")\n",
    "for gp in news:\n",
    "    print(\"   \" + gp)\n",
    "\n",
    "print(f\"\\nTotals:\\nnones: {len(nones)}\\nolds :{len(olds)}\\nnews :{len(news)}\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27519675-d8ca-413f-8ff2-86ad73102ddf",
   "metadata": {},
   "source": [
    "### 2b. Print curation status of a specific project\n",
    "Replace `\"my-group/my-bids-project\"` with the Flywheel address of your project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a3cf31f",
   "metadata": {},
   "outputs": [],
   "source": [
    "project = fw.lookup(\"my-group/my-bids-project\")\n",
    "get_project_bids_status(project, verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68f1c2aa",
   "metadata": {},
   "source": [
    "#### Example 2b-1: Project not curated"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b36eea43-04e7-471a-8907-76e85ba2b942",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Project new-project does not have 'BIDS' in project.info\n",
      "new-project\n",
      "{}\n"
     ]
    }
   ],
   "source": [
    "# Check the status of the project \"bids-curation-test/new-project\"\n",
    "project = fw.lookup(\"bids-curation-test/new-project\")\n",
    "get_project_bids_status(project, verbose=True)\n",
    "\n",
    "# Print out the project.label and project.info for \"bids-curation-test/new-project\"\n",
    "print(project.label)\n",
    "print(project.info)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b4023ac-fa71-4799-a05d-b01513e8060d",
   "metadata": {},
   "source": [
    "#### Example 2b-2: Project curated the \"new way\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "f4730cdc-f26d-4de3-b178-2846bc2bc552",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Project ReproIn_22 is curated the 'new way' (sidecar data is in sidecars)\n",
      "ReproIn_22\n",
      "{'BIDS': {'template': 'project', 'rule_id': 'bids_project', 'Sidecar': 'data is in json sidecar, not file.info'}}\n"
     ]
    }
   ],
   "source": [
    "# Check the status of the project \"bids-curation-test/ReproIn_22\"\n",
    "project = fw.lookup(\"bids-curation-test/ReproIn_22\")\n",
    "get_project_bids_status(project, verbose=True)\n",
    "\n",
    "# Print out the project.label and project.info for \"bids-curation-test/ReproIn_22\"\n",
    "print(project.label)\n",
    "print(project.info)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94be55dd-11fe-4fb6-9073-c10a9e083f01",
   "metadata": {},
   "source": [
    "#### Example 2b-3: Project curated the \"old way\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "6ca9d54c-49bb-472b-9d06-7824c288defc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Project Levitas_Tutorial is curated the 'old way' (sidecar data is in NIfTI file metadata)\n",
      "Levitas_Tutorial\n",
      "{'BIDS': {'template': 'project', 'rule_id': 'bids_project', 'Name': 'BIDS_multi_session', 'BIDSVersion': '1.0.2', 'License': '', 'Authors': [], 'Acknowledgements': '', 'HowToAcknowledge': '', 'Funding': [], 'ReferencesAndLinks': [], 'DatasetDOI': ''}}\n"
     ]
    }
   ],
   "source": [
    "# Check the status of the project \"bids-curation-test/Levitas_Tutorial\"\n",
    "project = fw.lookup(\"bids-curation-test/Levitas_Tutorial\")\n",
    "get_project_bids_status(project, verbose=True)\n",
    "\n",
    "# Print out the project.label and project.info for \"bids-curation-test/Levitas_Tutorial\"\n",
    "print(project.label)\n",
    "print(project.info)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dec7e85e",
   "metadata": {},
   "source": [
    "## Workflow 3: Set up new project to be curated\n",
    "For brand new projects, you may wish to preset the BIDS curation method before uploading data.  \n",
    "\n",
    "**Note**: For new projects created on instances running Flywheel v18+, projects need to be manually set to be curated the \"old\" \n",
    "way prior to running `fw ingest bids` to import an existing BIDS dataset (see workflow **3a**)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ec2e5c4-14c0-4179-9703-63a861c93599",
   "metadata": {},
   "source": [
    "### 3a. Set a new project to be curated the \"old way\"\n",
    "\n",
    "Sidecar data will be stored in NIfTI file metadata (file.info.XXX) and sidecars will be ignored if they exist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fae558e-af91-4848-80fe-16989b3dda73",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the address of the project to your project\n",
    "project = fw.lookup(\"my-group/my-bids-project\")\n",
    "set_project_old_new_none(fw, project.id, \"old\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "967bf13e-abb3-4407-8f5a-542655a9658e",
   "metadata": {},
   "outputs": [],
   "source": [
    "hc.curate_project(project, update_metadata=True, make_sidecar=False, way='old', report_only=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70f7fc89-ee48-47da-a172-e8223d441efb",
   "metadata": {},
   "source": [
    "### 3b. Set the project to be curated the \"new way\"\n",
    "\n",
    "This indicates that the project should be curated the \"new way\" when the curation gear is run or when the dcm2niix gear is run: \n",
    "sidecar data will be stored in sidecars, not in NIfTI file metadata."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19b37505-b0a2-4368-9a90-b4ede3fe7222",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the address of the project to your project\n",
    "project = fw.lookup(\"my-group/my-bids-project\")\n",
    "set_project_old_new_none(fw, project.id, \"new\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c07963b3-b413-49f2-aa37-68df8fddc85e",
   "metadata": {},
   "outputs": [],
   "source": [
    "hc.curate_project(project, update_metadata=False, make_sidecar=True, report_only=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6bd0584-d80f-4a3d-a821-36880543e81b",
   "metadata": {},
   "source": [
    "### 3c. Clear out (reset) the project.info.BIDS metadata\n",
    "\n",
    "This indicates that the project should be curated the \"new way\" when the curation gear is run or when the dcm2niix gear is run: sidecar \n",
    "data will be stored in sidecars, not in NIfTI file metadata.\n",
    "\n",
    "**Note**: This option should really only be run on projects where there are only DICOM files.  If nifti and JSON sidecar files already \n",
    "exist, use option **3b** to make sure that the project and all acquisitions and files are correctly curated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fcaaed0-2227-4cf0-a76b-bb7fc120faec",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the address of the project to your project\n",
    "project = fw.lookup(\"my-group/my-bids-project\")\n",
    "set_project_old_new_none(fw, project.id, \"none\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4924be7-971e-4d08-b3e0-87787bacbe7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "hc.curate_project(project, update_metadata=False, make_sidecar=False, way='none', report_only=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
