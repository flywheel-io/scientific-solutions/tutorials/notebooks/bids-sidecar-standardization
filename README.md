# BIDS sidecar standardization



Flywheel has standardized BIDS sidecar usage to align more fully with expectations about BIDS sidecar json files.  

Previously, sidecar data was stored in "Custom Information" on NIfTI files and the actual sidecar file was ignored. In the Flywheel SDK, this data shows up as metadata: `file.info.<key-value>` (the "old way").  

The more recent version of the dcm2niix gear keeps the actual JSON sidecar and does not put sidecar data in NIfTI file metadata.  The recent versions of the BIDS Curation gear and all BIDS App gears are backwards compatible and will work when sidecar data is stored either way.

This change was made because the "old way" often lead to confusion.

In order to provide compatibility with either method of storing the sidecar data, we offer this notebook to curate your project as you wish to see the sidecar data stored.

Please run the cells in this notebook, replacing any strings that are user specific (e.g., key alias, project hash) or upload the large code block to your block per the instructions provided in the notebook.

Please see [Storing BIDS Sidecars](https://docs.flywheel.io/User_Guides/user_bids_storing_sidecars/) for more information, including on how to use the Hierarchy Curator Gear to do the same things.
